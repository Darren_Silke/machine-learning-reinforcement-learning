#pragma once
#include "cdisccontroller.h"
#include "CParams.h"
#include "CDiscCollisionObject.h"
#include <cmath>
#include <random>

typedef unsigned int uint;

// Class used to represent an action that an agent takes.
// Possible actions that an agent can take are up, down, left and right.
class Action
{
public:
	// Constructor function.
	Action(ROTATION_DIRECTION action);

	// Value for action, initialized to 0.
	int value = 0;
	// Rotation direction used to store the action to move to the next state.
	ROTATION_DIRECTION action;
};

// Class used to represent a state that an agent can be in.
class State
{
public:
	// Constructor function.
	State();

	int xPosition;
	int yPosition;
	// Vector used to store state and action pair.
	std::vector<Action> stateAndAction;
};

// Class used to represent a Q Table for a particular agent, as well as the particular agent's current state and index for its next action.
class Sweeper
{
public:
	// Constructor function.
	Sweeper();

	int nextActionIndex = 0;
	State* currentState = nullptr;
	std::vector<std::vector<State> > qTable;
};

class CQLearningController : public CDiscController
{
private:
	uint _grid_size_x;
	uint _grid_size_y;
	// Used to store the total number of states.
	uint gridSquareSize;

	// Reward for transitioning to a super mine state.
	int rewardForSuperMine = -1000;
	// Reward for transitioning to a rock state.
	int rewardForRock = -1000;
	// Reward for transitioning to an empty state.
	int rewardForEmptyState = 0;
	// Reward for transitioning to a mine state.
	int rewardForMine = 1;

	double discountFactorGamma = 0.9;
	double learningRateEta = 0.8;

	float epsilon = 0.8f;

	// Vector used to store a Q Table for each Sweeper.
	std::vector<Sweeper> sweepers;
	
public:
	CQLearningController(HWND hwndMain);
	virtual void InitializeLearningAlgorithm(void);
	int R(uint x, uint y, uint sweeper_no);
	int indexForBestAction(State* currentState, bool findNextMaximum);
	virtual bool Update(void);
	virtual ~CQLearningController(void);
};
