Author: Darren Silke
Date: 19 October 2015

Name: Machine Learning - Artificial Neural Networks And Reinforcement Learning - Part 2

Description: See assignment instructions for details.

Instructions:

1. Open smart sweepers.vcxproj with Microsoft Visual Studio Professional 2013.
2. Build solution.
3. Run solution.
4. Evaluate output.

TAKE NOTE OF THE .git FOLDER WHICH CONTAINS ALL INFORMATION RELATING TO THE USE OF GIT FOR A LOCAL REPOSITORY AS REQUIRED BY THIS COURSE.
