/**
         (                                      
   (     )\ )                                   
 ( )\   (()/(   (    ) (        (        (  (   
 )((_)   /(_)) ))\( /( )(   (   )\  (    )\))(  
((_)_   (_))  /((_)(_)|()\  )\ |(_) )\ )((_))\  
 / _ \  | |  (_))((_)_ ((_)_(_/((_)_(_/( (()(_) 
| (_) | | |__/ -_) _` | '_| ' \)) | ' \)) _` |  
 \__\_\ |____\___\__,_|_| |_||_||_|_||_|\__, |  
                                        |___/   

Refer to Watkins, Christopher JCH, and Peter Dayan. "Q-learning." Machine learning 8. 3-4 (1992): 279-292
for a detailed discussion on Q Learning
*/
#include "CQLearningController.h"

Action::Action(ROTATION_DIRECTION action) : action(action)
{
	/*
	 * Deliberately left empty.
	 */
}

State::State()
{
	// RIGHT.
	stateAndAction.push_back(Action(EAST));
	// UP.
	stateAndAction.push_back(Action(NORTH));
	// LEFT.
	stateAndAction.push_back(Action(WEST));
	// DOWN.
	stateAndAction.push_back(Action(SOUTH));
}

Sweeper::Sweeper()
{
	/*
	 * Deliberately left empty.
	 */
}


CQLearningController::CQLearningController(HWND hwndMain):
	CDiscController(hwndMain),
	_grid_size_x(CParams::WindowWidth / CParams::iGridCellDim + 1),
	_grid_size_y(CParams::WindowHeight / CParams::iGridCellDim + 1)
{
	// Calculate the total number of states in the world.
	gridSquareSize = _grid_size_x * _grid_size_y;
}

/**
 The update method should allocate a Q table for each sweeper (this can
 be allocated in one shot - use an offset to store the tables one after the other)

 You can also use a boost multiarray if you wish
*/
void CQLearningController::InitializeLearningAlgorithm(void)
{
	std::cout << "Initializing application..." << std::endl;
	for (int i = 0; i < m_NumSweepers; i++)
	{
		Sweeper sweeper;
		for (int j = 0; j < _grid_size_x; j++)
		{
			std::vector<State> sweeperQTable;
			for (int k = 0; k < _grid_size_y; k++)
			{
				sweeperQTable.push_back(State());
			}
			sweeper.qTable.push_back(sweeperQTable);
		}
		sweepers.push_back(sweeper);
	}
	std::cout << "Done initializing application." << std::endl;
}

/**
 The immediate reward function. This computes a reward upon achieving the goal state of
 collecting all the mines on the field. It may also penalize movement to encourage exploring all directions and 
 of course for hitting supermines/rocks!
*/
int CQLearningController::R(uint x,uint y, uint sweeper_no)
{
	bool isEmpty = false;

	for (int i = 0; i < m_vecObjects.size(); i++)
	{
		// Calculate the current grid x and y positions from the world co-ordinates.
		int currentXPosition = m_vecObjects[i]->getPosition().x / CParams::iGridCellDim;
		int currentYPosition = m_vecObjects[i]->getPosition().y / CParams::iGridCellDim;

		// Check if there is a mine, super mine or rock in the current position and it is still there.
		if ((currentXPosition == x) && (currentYPosition == y))
		{
			isEmpty = true;

			// Check if it is a super mine.
			if (m_vecObjects[i]->getType() == CCollisionObject::ObjectType::SuperMine)
			{
				// The agents should be prevented from moving here again in the future, from any rotation(I.E.LEFT, RIGHT, UP or DOWN).
				sweepers[sweeper_no].qTable[x][y].stateAndAction[0].value = 0;
				sweepers[sweeper_no].qTable[x][y].stateAndAction[1].value = 0;
				sweepers[sweeper_no].qTable[x][y].stateAndAction[2].value = 0;
				sweepers[sweeper_no].qTable[x][y].stateAndAction[3].value = 0;
				
				return rewardForSuperMine;
			}
			// Check if it is a rock.
			else if (m_vecObjects[i]->getType() == CCollisionObject::ObjectType::Rock)
			{
				// The agents should be prevented from moving here again in the future, from any rotation(I.E.LEFT, RIGHT, UP or DOWN).
				sweepers[sweeper_no].qTable[x][y].stateAndAction[0].value = 0;
				sweepers[sweeper_no].qTable[x][y].stateAndAction[1].value = 0;
				sweepers[sweeper_no].qTable[x][y].stateAndAction[2].value = 0;
				sweepers[sweeper_no].qTable[x][y].stateAndAction[3].value = 0;
				
				return rewardForRock;
			}
			// Check if it is a mine.
			else if (m_vecObjects[i]->getType() == CCollisionObject::ObjectType::Mine)
			{
				// The agents should be prevented from moving here again in the future, from any rotation(I.E.LEFT, RIGHT, UP or DOWN).
				sweepers[sweeper_no].qTable[x][y].stateAndAction[0].value = 0;
				sweepers[sweeper_no].qTable[x][y].stateAndAction[1].value = 0;
				sweepers[sweeper_no].qTable[x][y].stateAndAction[2].value = 0;
				sweepers[sweeper_no].qTable[x][y].stateAndAction[3].value = 0;
				
				return rewardForMine;
			}

			// Prevented from searching through the rest of the objects.
			continue;
		}
	}
	// An empty position has been encountered (I.E. There is no mine, super mine or rock in the position).	
	return rewardForEmptyState;
}

// The index for best action function. 
// This function will return the index of the action that results in the biggest Q-value for a given state
// (I.E. The appropriate action that the agent should perform (LEFT, RIGHT, UP or DOWN)).
int CQLearningController::indexForBestAction(State* currentState, bool findNextMaximum)
{
	int indexForBestAction = 0;

	// Obtain a random probability number.
	std::random_device randomDevice;
	std::mt19937  mersenneTwister19937Generator(randomDevice());
	std::uniform_real_distribution<float> uniformRealDistribution(0, 1);
	float probability = uniformRealDistribution(mersenneTwister19937Generator);

	// The agent should perform a random action (I.E. Move LEFT, RIGHT, UP or DOWN)
	// only if the random probability number that was obtained is in the range of the epsilon exploration value.
	// The agent should explore it's environment.
	if ((findNextMaximum == false) && (probability <= epsilon))
	{
		// Obtain a random probability number.
		std::random_device randomDevice;
		std::mt19937 mersenneTwister19937Generator(randomDevice());
		std::uniform_int_distribution<int> uniformIntDistribution(0, 3);
		indexForBestAction = uniformIntDistribution(mersenneTwister19937Generator);

		return indexForBestAction;
	}
	// The agent should exploit it's environment.
	else
	{
		int bestAction = currentState->stateAndAction[0].value;
		int totalNumberOfEqualActions = 0;

		// Loop through the sweeper's possible actions that will take the agent to the next state, 
		// and select the action that yields the largest return.
		for (int i = 1; i < 4; i++)
		{
			int currentAction = currentState->stateAndAction[i].value;

			// If the current action value is larger than the current best action value,
			// then adjust the best action value and the index for the best action value accordingly.
			if (currentAction > bestAction)
			{
				bestAction = currentAction;
				indexForBestAction = i;
			}
			// If the current action value is equal to the current best action value,
			// then increase the total number of equal actions.
			else if (currentAction == bestAction)
			{
				totalNumberOfEqualActions++;
			}
		}

		// Finally, if all the action values are equal to one another, 
		// then the agent should select a random action to perform.
		if (totalNumberOfEqualActions == 3)
		{
			// Obtain a random probability number.
			std::random_device randomDevice;
			std::mt19937 ersenneTwister19937Generator(randomDevice());
			std::uniform_int_distribution<int> uniformIntDistribution(0, 3);
			indexForBestAction = uniformIntDistribution(ersenneTwister19937Generator);
		}

		return indexForBestAction;
	}
}

/**
The update method. Main loop body of our Q Learning implementation
See: Watkins, Christopher JCH, and Peter Dayan. "Q-learning." Machine learning 8. 3-4 (1992): 279-292
*/
bool CQLearningController::Update(void)
{
	// m_vecSweepers is the array of minesweepers.
	// Everything you need will be m_[something] ;)
	uint cDead = std::count_if(m_vecSweepers.begin(), m_vecSweepers.end(), [](CDiscMinesweeper * s)->bool
	{						
		return s->isDead();						   
	});

	// If all the sweepers are dead, skip to next iteration.
	if (cDead == CParams::iNumSweepers)
	{
		printf("All dead ... skipping to next iteration\n");
		m_iTicks = CParams::iNumTicks;
	}

	// If all the mines have been collected, skip to next iteration.
	if (m_minesGathered == CParams::iNumMines)
	{
		std::cout << "All mines obtained ... skipping to next iteration" << std::endl;
		m_iTicks = CParams::iNumTicks;
	}

	// The epsilon exploration factor should be decreased after every 50 iterations.
	if ((m_iIterations % 50) && (epsilon > 0.0f))
	{
		epsilon -= 0.2f;
	}

	for (uint sw = 0; sw < CParams::iNumSweepers; ++sw)
	{
		if (m_vecSweepers[sw]->isDead()) continue;

		/**
		Q-learning algorithm according to:
		Watkins, Christopher JCH, and Peter Dayan. "Q-learning." Machine learning 8. 3-4 (1992): 279-292
		*/
		
		//1:::Observe the current state:
		// This involves obtaining the current sweeper co-ordinates from the world co-ordinates.
		int currentXPosition = m_vecSweepers[sw]->Position().x / CParams::iGridCellDim;
		int currentYPosition = m_vecSweepers[sw]->Position().y / CParams::iGridCellDim;

		sweepers[sw].currentState = &sweepers[sw].qTable[currentXPosition][currentYPosition];
		sweepers[sw].currentState->xPosition = currentXPosition;
		sweepers[sw].currentState->yPosition = currentYPosition;

		//2:::Select action with highest historic return:
		// This involves the following two steps:
		// A. Obtain the next best action to perform.
		sweepers[sw].nextActionIndex = indexForBestAction(sweepers[sw].currentState, false);
		// B. Perform the next best action.
		m_vecSweepers[sw]->setRotation((ROTATION_DIRECTION)sweepers[sw].nextActionIndex);

		// Now call the parents update, so all the sweepers fulfill their chosen action.
	}
	
	CDiscController::Update(); // Call the parent's class update. Do not delete this.
	
	for (uint sw = 0; sw < CParams::iNumSweepers; ++sw)
	{
		if (m_vecSweepers[sw]->isDead()) continue;

		//3:::Observe new state:
		// Obtain previous state.
		int previousXPosition = sweepers[sw].currentState->xPosition;
		int previousYPosition = sweepers[sw].currentState->yPosition;
		// Obtain new current state.
		int newCurrentXPosition = m_vecSweepers[sw]->Position().x / CParams::iGridCellDim;
		int newCurrentYPosition = m_vecSweepers[sw]->Position().y / CParams::iGridCellDim;

		//4:::Update _Q_s_a accordingly:
		// This is done according to the following formula:
		// Q(s, a) = Q(s, a) + (eta * (R + gamma * MAX(Q(s', a')) - Q(s, a)))
		State* nextState = &sweepers[sw].qTable[newCurrentXPosition][newCurrentYPosition];
		int currentStateValue = sweepers[sw].currentState->stateAndAction[sweepers[sw].nextActionIndex].value;
		int bestActionIndex = indexForBestAction(nextState, true);
		int nextStateValue = nextState->stateAndAction[bestActionIndex].value;

		// Check if a mine, super mine or rock is found.
		// Then update the current state for the particular sweeper from all directions (I.E. LEFT, RIGHT, UP and DOWN).
		int reward = R(previousXPosition, previousYPosition, sw);
		if ((reward == 1) || (reward == -1000))
		{
			sweepers[sw].currentState->stateAndAction[0].value = currentStateValue + (learningRateEta * (reward + (discountFactorGamma * nextStateValue) - currentStateValue));
			sweepers[sw].currentState->stateAndAction[1].value = currentStateValue + (learningRateEta * (reward + (discountFactorGamma * nextStateValue) - currentStateValue));
			sweepers[sw].currentState->stateAndAction[2].value = currentStateValue + (learningRateEta * (reward + (discountFactorGamma * nextStateValue) - currentStateValue));
			sweepers[sw].currentState->stateAndAction[3].value = currentStateValue + (learningRateEta * (reward + (discountFactorGamma * nextStateValue) - currentStateValue));
		}
		else
		{
			sweepers[sw].currentState->stateAndAction[sweepers[sw].nextActionIndex].value = currentStateValue + learningRateEta * (reward + (discountFactorGamma * nextStateValue) - currentStateValue);
		}

		// Set the next state as the current state.
		sweepers[sw].currentState = nextState;
	}
	return true;
}

CQLearningController::~CQLearningController(void)
{
	/*
	 * Deliberately left empty.
	 */
}
